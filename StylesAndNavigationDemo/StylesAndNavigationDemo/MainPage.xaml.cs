﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using StylesAndNavigationDemo.BusinessLogic;

namespace StylesAndNavigationDemo
{
    public partial class MainPage : ContentPage
    {
        List<Product> _products = new List<Product>();
        public MainPage()
        {
            InitializeComponent();
            PkCategory.ItemsSource= Enum.GetValues(typeof(Category));
            BDelete.Clicked += BDelete_Clicked;
            BDelete.Clicked += BDelete_Clicked1;
        }

        private void OnAdd(object sender, EventArgs e)
        {
            //App.Current.Resources["ErrorColor"] = "Blue";
            int productId = int.Parse(TxtProductId.Text);
            string name = TxtProductName.Text;
            Category category = (Category)PkCategory.SelectedItem;
            int stock = int.Parse(TxtStock.Text);
            int price = int.Parse(TxtPrice.Text);

            Product product = new Product(productId, name, category, stock, price);
            _products.Add(product);
        }

        private void BDelete_Clicked1(object sender, EventArgs e)
        {
            DisplayAlert("Delete Clicked", "Second Handler", "Ok");
        }

        private void BDelete_Clicked(object sender, EventArgs e)
        {
            DisplayAlert("Delete Clicked", "First Handler", "Ok");
        }

        private async void OnViewAll(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ProductListPage(_products));
        }
    }
}
