﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using StylesAndNavigationDemo.BusinessLogic;

namespace StylesAndNavigationDemo
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProductListPage : ContentPage
    {
        Product _selectedProduct;
        public Product SelectedProduct
        {
            get => _selectedProduct;
            set
            {
                _selectedProduct = value;
                OnPropertyChanged("SelectedProduct");
            }
        }
        public ProductListPage(List<Product> products)
        {
            InitializeComponent();
            LstProducts.ItemsSource = products;
            BindingContext = this;
        }

        //private void OnProductSelected(object sender, SelectedItemChangedEventArgs e)
        //{
        //    //Navigation.PushAsync(new ProductDetailsPage((Product)LstProducts.SelectedItem));
        //}
    }
}