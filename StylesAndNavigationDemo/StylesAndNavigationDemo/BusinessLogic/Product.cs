﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StylesAndNavigationDemo.BusinessLogic
{
    public enum Category
    {
        Electronics,
        Food,
        Apparel,
    }
    public class Product
    {
        public int ProductId { get; private set; }
        public string ProductName { get; private set; }

        public Category ProductCategory { get; private set; }

        public int Stock { get; private set; }
        public int Price { get; private set; }

        public int StockValue => Stock * Price;

        public string CategoryIconFile => $"{ProductCategory.ToString().ToLower()}.png";
        public Product(int productId, string productName, Category category, int stock, int price)
        {
            ProductId = productId;
            ProductName = productName;
            ProductCategory = category;
            Stock = stock;
            Price = price;
        }

        public override string ToString() => $"{ProductId}, {ProductName}, {ProductCategory}";
        

    }
}
